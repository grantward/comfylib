//Provides dynamic type containers

#include <string.h>

#include "clib_dynamic.h"


void clib_dynamic_destroy(CLIB_DYNAMIC_T *d) {
    if (!d)
        return;
    switch (d->code) {
        //Determine inner malloc'd behavior based on type
        //Simple types do not need any additional frees
        case CLIB_DYNAMIC_STR:
            free(d->data_str);
            break;
    }
    free(d);
}

CLIB_DYNAMIC_T *clib_dynamic_str_create(char *str) {
    struct clib_dynamic_t *ret = calloc(1, sizeof(*ret));
    if (ret) {
        ret->code = CLIB_DYNAMIC_STR;
        ret->data_str = calloc(strlen(str)+1, sizeof(*str));
        if (!ret->data_str) {
            free(ret);
            return NULL;
        }
        strcpy(ret->data_str, str);
    }
    return ret;
}

#define __err_type      int64_t
#define __int_type      int64_t
#define __uint_type     uint64_t
#define __float_type    double

#define __err_code      CLIB_DYNAMIC_ERR
#define __int_code      CLIB_DYNAMIC_INT
#define __uint_code     CLIB_DYNAMIC_UINT
#define __float_code    CLIB_DYNAMIC_FLOAT

#define __CREATE_TYPE(type)                         \
    CLIB_DYNAMIC_T *clib_dynamic_##type##_create(__##type##_type data) {\
        struct clib_dynamic_t *ret = calloc(1, sizeof(*ret));   \
        if (!ret)                                   \
            return ret;                             \
        ret->code = __##type##_code;                \
        ret->data_##type = (__##type##_type)data;   \
        return ret;                                 \
    }

#define __REGISTER_TYPE(type)                       \
    __CREATE_TYPE(type)

__REGISTER_TYPE(err)
__REGISTER_TYPE(int)
__REGISTER_TYPE(uint)
__REGISTER_TYPE(float)
