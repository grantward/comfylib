//Provides implementation of string list

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "clib_list_string.h"


CLIB_LIST *clib_list_string_create(void) {
    return clib_list_create((void *)strdup, free);
}

void clib_list_string_print(CLIB_LIST *l) {
    clib_list_iterate(l, (void *)puts);
}
