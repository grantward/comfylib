//Implements indexed BST
//Essentially arbitrary data labelled with a static value

#include <stdlib.h>
#include <string.h>

#include "clib_bst.h"
#include "clib_bst_index.h"

struct clib_bst_index_data *clib_bst_index_copy(
        const struct clib_bst_index_data_c *data) {
    if (!data)
        return NULL;
    struct clib_bst_index_data *ret = calloc(1, sizeof(*ret));
    if (ret) {
        ret->index = malloc(data->index_size);
        if (!ret->index) {
            free(ret);
            return NULL;
        }
        ret->data = malloc(data->data_size);
        if (!ret->data) {
            free(ret->index);
            free(ret);
            return NULL;
        }
        memcpy(ret->index, data->index, data->index_size);
        memcpy(ret->data, data->data, data->data_size);
    }
    return ret;
}

void clib_bst_index_destroy(CLIB_BST_INDEX_DATA *data) {
    if (!data)
        return;
    free(data->index);
    free(data->data);
    free(data);
}

int clib_bst_index_add(CLIB_BST *b,
        size_t index_size, const void *index,
        size_t data_size, const void *data)
{
    struct clib_bst_index_data_c to_add;
    to_add.index_size = index_size;
    to_add.index = index;
    to_add.data_size = data_size;
    to_add.data = data;
    return clib_bst_add_node(b, &to_add);
}

int clib_bst_index_add_free(CLIB_BST *b,
        size_t index_size, void *index,
        size_t data_size, void *data)
{
    int ret = clib_bst_index_add(b, index_size, index, data_size, data);
    free(index);
    free(data);
    return ret;
}

int clib_bst_index_get(CLIB_BST *b, void **res, void *index) {
    struct clib_bst_index_data find;
    struct clib_bst_index_data *found;
    find.index = index;
    find.data = NULL;
    int ret = clib_bst_get(b, (void **)&found, &find);
    *res = found->data;
    return ret;
}

int clib_bst_index_get_free(CLIB_BST *b, void **res, void *index) {
    int ret = clib_bst_index_get(b, res, index);
    free(index);
    return ret;
}
