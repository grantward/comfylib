//Implements algorithms for CLIB_LISTs

#include <stdlib.h>

#include "clib.h"


int clib_list_quickselect(
        CLIB_LIST *l, void **res, int (*cmp)(void *, void *), size_t k) 
{
    if (!l || !cmp)
        return CLIB_ERR_NULL;
    ssize_t k_progress = k;
    if (k_progress >= clib_list_len(l))
        return CLIB_ERR_INDEX;

    //The subsection of list under test
    CLIB_LIST *slice = clib_list_shallow_copy(l);
    for ( ; clib_list_len(slice) > 1; ) {
        //Get pivot data
        void *data;
        clib_list_index(slice, &data, random() % clib_list_len(slice));
        //Pivot off data
        struct clib_list_shallow_cmp binned;
        binned = clib_list_shallow_copy_func_1_cmp(slice, cmp, data);
        clib_list_destroy(slice);
        ssize_t lesser_len = clib_list_len(binned.lesser);
        ssize_t equal_len = clib_list_len(binned.equal);
        if (lesser_len > k_progress) {
            //k < pivot
            slice = binned.lesser;
            clib_list_destroy(binned.equal);
            clib_list_destroy(binned.greater);
        } else if (lesser_len + equal_len > k_progress)
        {
            //k == pivot
            k_progress -= lesser_len;
            slice = binned.equal;
            clib_list_destroy(binned.lesser);
            clib_list_destroy(binned.greater);
        } else {
            //k > pivot
            k_progress -= (lesser_len+equal_len);
            slice = binned.greater;
            clib_list_destroy(binned.lesser);
            clib_list_destroy(binned.equal);
        }
    }
    *res = slice->first->data;
    clib_list_destroy(slice);

    return 0;
}

int clib_list_min(CLIB_LIST *l, void **res, int (*cmp)(void *,void *)) {
    //Select k=0 (minimum)
    //return clib_list_quickselect(l, res, cmp, 0);
    if (!l)
        return CLIB_ERR_NULL;
    void *min = NULL;
    if (l->first)
        min = l->first->data;
    for (struct clib_list_node *cur=l->first; cur; cur=cur->next) {
        if (cmp(cur->data, min) < 0)
            min = cur->data;
    }
    *res = min;
    return 0;
}

int clib_list_max(CLIB_LIST *l, void **res, int (*cmp)(void *,void *)) {
    //Select k=last
    //return clib_list_quickselect(l, res, cmp, clib_list_len(l)-1);
    if (!l)
        return CLIB_ERR_NULL;
    void *max = NULL;
    if (l->first)
        max = l->first->data;
    for (struct clib_list_node *cur=l->first; cur; cur=cur->next) {
        if (cmp(cur->data, max) > 0)
            max = cur->data;
    }
    *res = max;
    return 0;
}

int clib_list_median(
        CLIB_LIST *l, void **res,
        int (*cmp)(void *,void *),
        void *(*add)(void *,void *),
        void *(*div)(void *,ssize_t))
{
    if (clib_list_len(l) % 2)
        return clib_list_medianhigh(l, res, cmp);
    void *low, *high;
    int err = clib_list_medianlow(l, &low, cmp);
    if (err)
        return err;
    err = clib_list_medianhigh(l, &high, cmp);
    if (err)
        return err;
    void *sum = l->copy_data(low);
    sum = add(sum, high);
    *(res) = div(sum, 2);
    return 0;
}

int clib_list_medianlow(
        CLIB_LIST *l, void **res, int (*cmp)(void *,void *))
{
    ssize_t len = clib_list_len(l);
    size_t k = len%2 ? len/2 : len/2 - 1;
    return clib_list_quickselect(l, res, cmp, k);
}

int clib_list_medianhigh(
        CLIB_LIST *l, void **res, int (*cmp)(void *,void *))
{
    return clib_list_quickselect(l, res, cmp, clib_list_len(l)/2);
}
