//Provides a generic binary search tree structure

#include <stdarg.h>
#include <stdlib.h>

#include "clib.h"
#include "clib_bst.h"

CLIB_BST_NODE *clib_bst_node_create(
        const void *data, void *(*copy_data)(const void *)) {
    struct clib_bst_node *ret = calloc(1, sizeof(*ret));
    if (ret) {
        if (copy_data)
            ret->data = copy_data(data);
    }
    return ret;
}

CLIB_BST *clib_bst_create(
        void *(*copy_data)(const void *),
        void (*destroy_data)(void *),
        int (*cmp_data)(const void *,const void *)) {
    struct clib_bst *ret = calloc(1, sizeof(*ret));
    if (ret) {
        ret->copy_data = copy_data;
        ret->destroy_data = destroy_data;
        ret->cmp_data = cmp_data;
    }
    return ret;
}

//Destroys current node and recurses down left/right paths
void clib_bst_node_destroy(CLIB_BST_NODE *n, void (*destroy_data)(void *)) {
    if (!n)
        return;
    destroy_data(n->data);
    clib_bst_node_destroy(n->left, destroy_data);
    clib_bst_node_destroy(n->right, destroy_data);
    free(n);
}

void clib_bst_destroy(CLIB_BST *b) {
    if (!b)
        return;
    clib_bst_node_destroy(b->root, b->destroy_data);
    free(b);
}

//Add a node of data to the tree
int clib_bst_add_node(CLIB_BST *b, const void *data) {
    if (!b)
        return CLIB_ERR_NULL;
    struct clib_bst_node *to_add;
    to_add = clib_bst_node_create(data, b->copy_data);
    if (!to_add)
        return CLIB_ERR_MEM;
    struct clib_bst_node *parent = clib_bst_get_parent(b, data);
    if (!parent) {
        //Empty tree
        b->root = to_add;
    } else {
        //Tree has items
        //Find if new node is to left or right of parent then add
        to_add->parent = parent;
        int res = b->cmp_data(data, parent->data);
        if (res < 0) {
            parent->left = to_add;
        } else {
            parent->right = to_add;
        }
    }
    return 0;
}

int clib_bst_get(CLIB_BST *b, void **res, const void *data) {
    if (!b)
        return CLIB_ERR_NULL;
    struct clib_bst_node *found = clib_bst_get_node(b, data);
    if (!found)
        return CLIB_RES_NOT_FOUND;

    *(res) = found->data;
    return 0;
}

//Find and return the node for a value, or NULL if not found
CLIB_BST_NODE *clib_bst_get_node(CLIB_BST *b, const void *data) {
    if (!b)
        return NULL;
    struct clib_bst_node *cur = b->root;
    while (cur) {
        int res = b->cmp_data(data, cur->data);
        if (res == 0) {
            //Found node
            return cur;
        } else if (res < 0) {
            cur = cur->left;
        } else {
            cur = cur->right;
        }
    }
    return cur;
}

//Find and return the parent node for a value, or NULL if root
CLIB_BST_NODE *clib_bst_get_parent(CLIB_BST *b, const void *data) {
    if (!b)
        return NULL;
    struct clib_bst_node *parent=b->root, *child=NULL;
    while (parent) {
        int res = b->cmp_data(data, parent->data);
        if (res < 0) {
            //Lesser data, go left
            child = parent->left;
        } else {
            //Greater/equal, go right
            child = parent->right;
        }
        if (!child)
            return parent;
        parent = child;
    }
    return parent;
}
