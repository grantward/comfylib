//Provides simple type manipulation from generic memory locations

#include <stdio.h>

#include "clib_simple.h"


#define __TYPE_SIZE(type)                   \
    size_t clib_simple_##type##_size(type data) {\
        return sizeof(data);                \
    }

#define __TYPE_COPY(type)                   \
    void *clib_simple_##type##_copy(type *data) {\
        if (!data) {return data;}           \
        type *ret = malloc(sizeof(*ret));   \
        if (ret) {*ret = *data;}            \
        return ret;                         \
    }

#define __TYPE_STR(type)                    \
    char *clib_simple_##type##_str(type *data) {\
        char *ret = malloc(CLIB_SIMPLE_STRING_LEN * sizeof(*ret));\
        if (ret) {                          \
            snprintf(ret, CLIB_SIMPLE_STRING_LEN-1, CLIB_SIMPLE_##type##_str, *data);\
        }                                   \
        return ret;                         \
    }

#define __DECLARE(type)                     \
    __TYPE_SIZE(type)                       \
    __TYPE_COPY(type)                       \
    __TYPE_STR(type)

__DECLARE(int)
__DECLARE(long)
__DECLARE(float)
__DECLARE(double)

__DECLARE(uint8_t)
__DECLARE(uint16_t)
__DECLARE(uint32_t)
__DECLARE(uint64_t)
__DECLARE(int8_t)
__DECLARE(int16_t)
__DECLARE(int32_t)
__DECLARE(int64_t)
