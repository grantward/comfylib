//Implements the testing framework

#include <stdio.h>
#include <string.h>

#include "clib_test.h"

static void *_copy_test(struct clib_test *data) {
    if (!data)
        return data;
    struct clib_test *test_in = data;
    struct clib_test *ret = calloc(1, sizeof(*ret));
    if (!ret)
        return ret;
    ret->success = test_in->success;
    ret->test_name = calloc(strlen(test_in->test_name)+1, sizeof(char));
    if (!ret->test_name) {
        free(ret);
        return NULL;
    }
    strcpy(ret->test_name, test_in->test_name);
    return ret;
}

static void _destroy_test(struct clib_test *test) {
    if (!test)
        return;
    free(test->test_name);
    free(test);
}

static int _add_test(CLIB_LIST *l, char *test_name, int success) {
    if (!l)
        return CLIB_ERR_NULL;
    if (success)
        //Filter integers to 0/1
        success = 1;
    if (!test_name)
        //Set default test name
        test_name = "Unnamed test";
    struct clib_test build;
    build.success = success;
    build.test_name = test_name;
    return clib_list_append(l, &build);
}

static int _get_success(struct clib_test *test) {
    return test->success;
}

static void _print_result(struct clib_test *test) {
    if (test->success) {
        printf("PASS: %s\n", test->test_name);
    } else {
        printf("FAIL: %s\n", test->test_name);
    }
}

CLIB_LIST *clib_test_create(void) {
    return clib_list_create((void *)_copy_test, (void *)_destroy_test);
}

int clib_test_equal(CLIB_LIST *l, char *test_name,
        int expr_1, int expr_2) {
    int success = expr_1 == expr_2;
    return _add_test(l, test_name, success);
}

void clib_test_print_results(CLIB_LIST *l) {
    if (!l)
        return;
    ssize_t total_tests = clib_list_len(l);
    CLIB_LIST *test_success = clib_list_int_map(l, (void *)_get_success);
    int successes = clib_list_int_sum(test_success);
    clib_list_destroy(test_success);
    
    //Print test results
    clib_list_iterate(l, (void *)_print_result);
    //Print totals
    printf("Total tests run:    %zi\n", total_tests);
    printf("Successful:         %i\n", successes);
    printf("Failed:             %zi\n", total_tests-successes);
}
