//Provides a shallow copy functionality for clib_lists

#include "clib_list_shallow.h"


static int _always_true(void) {
    return 1;
}

static void *_shallow_copy(void *data) {
    return data;
}

static void _shallow_destroy(void) {
    return;
}

CLIB_LIST *clib_list_shallow_copy(CLIB_LIST *l) {
    return clib_list_shallow_copy_func(l, (void *)_always_true);
}

CLIB_LIST *clib_list_shallow_copy_func(CLIB_LIST *l, int (*func)(void *)) {
    struct clib_list *ret;
    ret = clib_list_create(_shallow_copy, (void *)_shallow_destroy);
    if (!ret)
        return ret;
    for (struct clib_list_node *cur=l->first; cur; cur=cur->next) {
        if (func(cur->data))
            clib_list_append(ret, cur->data);
    }
    return ret;
}

CLIB_LIST *clib_list_shallow_copy_func_1(
        CLIB_LIST *l,
        int (*func)(void *, void *),
        void *arg_1)
{
    struct clib_list *ret;
    ret = clib_list_create(_shallow_copy, (void *)_shallow_destroy);
    if (!ret)
        return ret;
    for (struct clib_list_node *cur=l->first; cur; cur=cur->next) {
        if (func(cur->data, arg_1))
            clib_list_append(ret, cur->data);
    }
    return ret;
}

struct clib_list_shallow_true_false clib_list_shallow_copy_func_1_both(
        CLIB_LIST *l,
        int (*func)(void *, void *),
        void *arg_1)
{
    struct clib_list_shallow_true_false ret;
    ret.true = clib_list_create(_shallow_copy, (void *)_shallow_destroy);
    ret.false = clib_list_create(_shallow_copy, (void *)_shallow_destroy);
    if (!ret.true || !ret.false) {
        free(ret.true);
        ret.true = NULL;
        free(ret.false);
        ret.false = NULL;
        return ret;
    }
    for (struct clib_list_node *cur=l->first; cur; cur=cur->next) {
        if (func(cur->data, arg_1)) {
            clib_list_append(ret.true, cur->data);
        } else {
            clib_list_append(ret.false, cur->data);
        }
    }
    return ret;
}

struct clib_list_shallow_cmp clib_list_shallow_copy_func_1_cmp(
        CLIB_LIST *l,
        int (*cmp)(void *, void *),
        void *arg_1)
{
    struct clib_list_shallow_cmp ret;
    ret.lesser = clib_list_create(_shallow_copy, (void *)_shallow_destroy);
    ret.equal = clib_list_create(_shallow_copy, (void *)_shallow_destroy);
    ret.greater = clib_list_create(_shallow_copy, (void *)_shallow_destroy);
    if (!ret.lesser || !ret.equal || !ret.greater) {
        clib_list_destroy(ret.lesser);
        clib_list_destroy(ret.equal);
        clib_list_destroy(ret.greater);
        ret.lesser = NULL;
        ret.equal = NULL;
        ret.greater = NULL;
        return ret;
    }
    for (struct clib_list_node *cur=l->first; cur; cur=cur->next) {
        int res = cmp(cur->data, arg_1);
        if (res < 0) {
            clib_list_append(ret.lesser, cur->data);
        } else if (res > 0) {
            clib_list_append(ret.greater, cur->data);
        } else {
            clib_list_append(ret.equal, cur->data);
        }
    }
    return ret;
}
