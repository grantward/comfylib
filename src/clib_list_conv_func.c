#include <stdio.h>

#include "clib_list_conv_func.h"


//Simple type convenience functions for lists
#define _CLIB__TYPE_COPY(type)                      \
    static void *_copy_##type(void *data) {         \
        if (!data)                                  \
            return data;                            \
        type *ret = malloc(sizeof(type));           \
        if (ret)                                    \
            *ret = *(type *)data;                   \
        return ret;                                 \
    }

#define _CLIB__TYPE_ADD(type)                           \
    static void *_add_##type(void *res, void *data) {   \
        *(type *)res += *(type *)data;                  \
        return res;                                     \
    }

#define _CLIB__TYPE_DIV(type)                           \
    static void *_div_##type(void *res, ssize_t len) {  \
        *(type *)res /= (type)len;                      \
        return res;                                     \
    }

#define _CLIB__TYPE_CMP(type)                           \
    static int _cmp_##type(void *a, void *b) {          \
        type der_a = *(type *)a, der_b = *(type *)b;    \
        if (der_a < der_b) {                            \
            return -1;                                  \
        } else if (der_a > der_b) {                     \
            return 1;                                   \
        } else {                                        \
            return 0;                                   \
        }                                               \
    }

#define _CLIB__TYPE_INSERT(type)                        \
    int clib_list_##type##_insert(CLIB_LIST *l, ssize_t index, type data) {\
        return clib_list_insert(l, index, &data);       \
    }

#define _CLIB__TYPE_APPEND(type)                                \
    int clib_list_##type##_append(CLIB_LIST *l, type data) {    \
        return clib_list_append(l, &data);                      \
    }

#define _CLIB__TYPE_APPENDLEFT(type)                    \
    int clib_list_##type##_appendleft(CLIB_LIST *l, type data) {    \
        return clib_list_appendleft(l, &data);          \
    }

#define _CLIB__TYPE_POP_INDEX(type)                         \
    int clib_list_##type##_pop_index(CLIB_LIST *l, type *res, ssize_t index)   \
    {                                                       \
        void *temp;                                         \
        int ret = clib_list_pop_index(l, &temp, index);     \
        if (ret)                                            \
            return ret;                                     \
        *res = *(type *)temp;                               \
        return ret;                                         \
    }

#define _CLIB__TYPE_POP(type)                               \
    int clib_list_##type##_pop(CLIB_LIST *l, type *res) {   \
        return clib_list_##type##_pop_index(l, res, -1);    \
    }
#define _CLIB__TYPE_POPLEFT(type)                           \
    int clib_list_##type##_popleft(CLIB_LIST *l, type *res) {   \
        return clib_list_##type##_pop_index(l, res, 0);    \
    }

#define _CLIB__TYPE_CREATE(type)                        \
    CLIB_LIST *clib_list_##type##_create(void) {        \
        return clib_list_create(_copy_##type, free);    \
    }

#define _CLIB__TYPE_INDEX(type)                                 \
    int clib_list_##type##_index(CLIB_LIST *l, type *res, ssize_t index) {\
        void *temp;                                             \
        int ret = clib_list_index(l, &temp, index);             \
        if (ret)                                                \
            return ret;                                         \
        *res = *(type *)temp;                                   \
        return ret;                                             \
    }

#define _CLIB__TYPE_MAP(type)                           \
    CLIB_LIST *clib_list_##type##_map(CLIB_LIST *l, type (*func)(void *)) {\
        if (!l || !func)                                \
            return NULL;                                \
        CLIB_LIST *ret = clib_list_##type##_create();   \
        for (struct clib_list_node *cur=l->first; cur; cur=cur->next) {\
            clib_list_##type##_append(ret, func(cur->data)); \
        }                                               \
        return ret;                                     \
    }

#define _CLIB__TYPE_TO_JSON(type)                   \
    char *clib_list_##type##_to_json(CLIB_LIST *l) {\
        return clib_list_to_json(l, (void *)clib_simple_##type##_str);   \
    }

#define _CLIB__TYPE_QUICKSELECT(type)               \
    int clib_list_##type##_quickselect(CLIB_LIST *l, type *res, size_t k) {\
        void *temp;                                 \
        int ret = clib_list_quickselect(l, &temp, _cmp_##type, k);\
        *res = *(type *)temp;                       \
        return ret;                                 \
    }                                               \
    int clib_list_##type##_min(CLIB_LIST *l, type *res) {\
        void *temp;                                 \
        int ret = clib_list_min(l, &temp, _cmp_##type);\
        *res = *(type *)temp;                       \
        return ret;                                 \
    }                                               \
    int clib_list_##type##_max(CLIB_LIST *l, type *res) {\
        void *temp;                                 \
        int ret = clib_list_max(l, &temp, _cmp_##type);\
        *res = *(type *)temp;                       \
        return ret;                                 \
    }

#define _CLIB__TYPE_MEDIAN(type)                    \
    int clib_list_##type##_median(CLIB_LIST *l, type *res) {\
        void *temp;                                 \
        int ret = clib_list_median(                 \
                l, &temp, _cmp_##type, _add_##type, _div_##type);\
        *(res) = *(type *)temp;                     \
        free(temp);                                 \
        return ret;                                 \
    }

#define _CLIB__TYPE_SUM(type)                       \
    type clib_list_##type##_sum(CLIB_LIST *l) {     \
        type res = 0;                               \
        clib_list_sum(l, _add_##type, &res);        \
        return res;                                 \
    }

#define _CLIB__TYPE_MEAN(type)                              \
    type clib_list_##type##_mean(CLIB_LIST *l) {            \
        type res = 0;                                       \
        clib_list_mean(l, _add_##type, _div_##type, &res);  \
        return res;                                         \
    }

//Define all convenience functions for a simple type
#define _CLIB__TYPE(type)       \
    _CLIB__TYPE_COPY(type)      \
    _CLIB__TYPE_ADD(type)       \
    _CLIB__TYPE_DIV(type)       \
    _CLIB__TYPE_CMP(type)       \
    _CLIB__TYPE_INSERT(type)    \
    _CLIB__TYPE_APPEND(type)    \
    _CLIB__TYPE_APPENDLEFT(type)\
    _CLIB__TYPE_POP_INDEX(type) \
    _CLIB__TYPE_POP(type)       \
    _CLIB__TYPE_POPLEFT(type)   \
    _CLIB__TYPE_CREATE(type)    \
    _CLIB__TYPE_INDEX(type)     \
    _CLIB__TYPE_MAP(type)       \
    _CLIB__TYPE_TO_JSON(type)   \
    _CLIB__TYPE_QUICKSELECT(type)\
    _CLIB__TYPE_MEDIAN(type)    \
    _CLIB__TYPE_SUM(type)       \
    _CLIB__TYPE_MEAN(type)

//Types
//Must also be declared in header file
_CLIB__TYPE(int)
_CLIB__TYPE(long)
_CLIB__TYPE(float)
_CLIB__TYPE(double)

_CLIB__TYPE(uint8_t)
_CLIB__TYPE(uint16_t)
_CLIB__TYPE(uint32_t)
_CLIB__TYPE(uint64_t)
_CLIB__TYPE(int8_t)
_CLIB__TYPE(int16_t)
_CLIB__TYPE(int32_t)
_CLIB__TYPE(int64_t)
