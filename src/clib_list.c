//Provides implementation for clib structures and functions

#include <string.h>

#include "clib.h"


CLIB_LIST_NODE *clib_list_node_create(
        void *data, void *(*copy_data)(void *)) {
    struct clib_list_node *ret;
    ret = calloc(1, sizeof(*ret));
    if (ret) {
        ret->data = copy_data(data);
    }
    return ret;
}

CLIB_LIST *clib_list_create(
        void *(*copy_data)(void *),
        void (*destroy_data)(void *)) {
    struct clib_list *ret;    
    ret = calloc(1, sizeof(*ret));
    if (ret) {
        ret->copy_data = copy_data;
        ret->destroy_data = destroy_data;
    }
    return ret;
}

void clib_list_node_destroy(CLIB_LIST_NODE *n, void (*destroy_data)(void *)) {
    if (!n) {
        return;
    }
    if (destroy_data)
        destroy_data(n->data);
    free(n);
}

void clib_list_destroy(CLIB_LIST *l) {
    if (!l) {
        return;
    }
    struct clib_list_node *cur = l->first;
    struct clib_list_node *next;
    while (cur) {
        next = cur->next;
        clib_list_node_destroy(cur, l->destroy_data);
        cur = next;
    }
    free(l);
}

int clib_list_insert(CLIB_LIST *l, ssize_t index, void *data) {
    if (!l)
        return CLIB_ERR_NULL;

    struct clib_list_node *new = clib_list_node_create(data, l->copy_data);
    if (!new)
        return CLIB_ERR_MEM;

    struct clib_list_node *insert_at = clib_list_get_node(l, index);

    new->next = insert_at;
    if (insert_at) {
        //Not last
        new->prev = insert_at->prev;
        insert_at->prev = new;
    } else {
        //last
        new->prev = l->last;
        l->last = new;
    }
    if (new->prev) {
        //Not first
        new->prev->next = new;
    } else {
        //First
        l->first = new;
    }
    l->len += 1;

    return 0;
}

int clib_list_append(CLIB_LIST *l, void *data) {
    //return clib_list_insert(l, clib_list_len(l)+1, data);
    if (!l)
        return CLIB_ERR_NULL;
    struct clib_list_node *new = clib_list_node_create(data, l->copy_data);
    if (!new)
        return CLIB_ERR_MEM;
    struct clib_list_node *prev = l->last;
    l->last = new;
    new->prev = prev;
    if (prev) {
        prev->next = new;
    } else {
        l->first = new;
    }
    l->len += 1;
    return 0;
}

int clib_list_appendleft(CLIB_LIST *l, void *data) {
    return clib_list_insert(l, 0, data);
}

int clib_list_pop_index(CLIB_LIST *l, void **res, ssize_t index) {
    int err = clib_list_index(l, res, index);
    if (err)
        return err;
    return clib_list_node_delete(l, index);
}

int clib_list_pop(CLIB_LIST *l, void **res) {
    return clib_list_pop_index(l, res, -1);
}

int clib_list_popleft(CLIB_LIST *l, void **res) {
    return clib_list_pop_index(l, res, 0);
}

int clib_list_node_delete(CLIB_LIST *l, ssize_t index) {
    if (!l)
        return CLIB_ERR_NULL;

    struct clib_list_node *destroy = clib_list_get_node(l, index);
    if (!destroy) {
        return CLIB_ERR_INDEX;
    }

    if (destroy->prev) {
        //Not first
        destroy->prev->next = destroy->next;
    } else {
        //First
        l->first = destroy->next;
    }
    if (destroy->next) {
        //Not last
        destroy->next = destroy->prev;
    } else {
        //Last
        l->last = destroy->prev;
    }
    clib_list_node_destroy(destroy, l->destroy_data);
    l->len -= 1;

    return 0;
}

ssize_t clib_list_len(CLIB_LIST *l) {
    if (!l)
        return CLIB_ERR_NULL;

    //size_t len = 0;
    //for (struct clib_list_node *cur=l->first; cur; cur=cur->next) {
    //    len++;
    //}
    return l->len;
}

int clib_list_index(CLIB_LIST *l, void **res, ssize_t index) {
    if (!l)
        return CLIB_ERR_NULL;
    struct clib_list_node *node;
    node = clib_list_get_node(l, index);
    if (!node)
        return CLIB_ERR_INDEX;
    *res = node->data;
    //*res = l->copy_data(node->data);
    return 0;
}

CLIB_LIST_NODE *clib_list_get_node(CLIB_LIST *l, ssize_t index) {
    if (!l)
        return NULL;

    struct clib_list_node *node;
    if (index >= 0) {
        node = l->first;
    } else {
        node = l->last;
    }
    while (node && index != 0 && index != -1) {
        if (index >= 0) {
            node = node->next;
            index -= 1;
        } else {
            node = node->prev;
            index += 1;
        }
    }
    return node;
}

void clib_list_iterate(CLIB_LIST *l, void (*func)(void *)) {
    if (!l || !func)
        return;
    for (struct clib_list_node *cur=l->first; cur; cur=cur->next) {
        func(cur->data);
    }
}

CLIB_LIST *clib_list_map(
        CLIB_LIST *l,
        void *(*func)(const void *),
        void *(*copy_data)(void *),
        void (*destroy_data)(void *))
{
    if (!l)
        return NULL;
    struct clib_list *ret = clib_list_create(copy_data, destroy_data);
    if (!ret)
        return ret;
    void *temp = NULL;
    for (struct clib_list_node *cur=l->first; cur; cur=cur->next) {
        if (func)
            temp = func(cur->data);
        clib_list_append(ret, temp);
        if (destroy_data)
            destroy_data(temp);
    }
    return ret;
}

char *clib_list_to_json(CLIB_LIST *l, char *(*func)(void *)) {
    if (!l || !func)
        return NULL;
    CLIB_STRING_GROWABLE *ret = clib_string_growable_create();
    if (!ret)
        return NULL;

    clib_string_growable_append(ret, "[");
    size_t start_len = ret->length;
    char *temp;
    for (struct clib_list_node *cur=l->first; cur; cur=cur->next) {
        temp = func(cur->data);
        if (!temp || !strlen(temp)) {
            free(temp);
            continue;
        }
        if (ret->length > start_len)
            //Item preceeds this item, add comma
            clib_string_growable_append(ret, ",");
        clib_string_growable_append(ret, temp);
        free(temp);
    }
    clib_string_growable_append(ret, "]");

    return clib_string_growable_extract(ret);
}

void *clib_list_sum(CLIB_LIST *l, void *(*sum_func)(void *, void *), 
        void *res) {
    if (!l)
        return res;
    for (struct clib_list_node *cur=l->first; cur; cur=cur->next) {
        res = sum_func(res, cur->data);
    }
    return res;
}

void *clib_list_mean(
        CLIB_LIST *l,
        void *(*sum_func)(void *,void *),
        void *(*div_func)(void *,ssize_t),
        void *res)
{
    if (!l)
        return res;
    res = clib_list_sum(l, sum_func, res);
    res = div_func(res, clib_list_len(l));
    return res;
}
