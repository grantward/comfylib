//Implementation of BST for common simple types

#include "clib_bst_simple.h"

int clib_bst_index_string_cmp(
        const CLIB_BST_INDEX_DATA *a,
        const CLIB_BST_INDEX_DATA *b)
{
    return strcmp((char *)a->index, (char *)b->index);
}

int clib_bst_index_string_get(CLIB_BST *b, char **res, void *index) {
    return clib_bst_index_get_free(b, (void **)res, index);
}

#define __INDEX_CMP(type)                                       \
    int clib_bst_index_##type##_cmp(                            \
            const CLIB_BST_INDEX_DATA *container_a,             \
            const CLIB_BST_INDEX_DATA *container_b) {           \
        type index_a = *(type *)(container_a->index);           \
        type index_b = *(type *)(container_b->index);           \
        if (index_a < index_b) {return -1;}                     \
        else if (index_a > index_b) {return 1;}                 \
        else {return 0;}                                        \
    }

#define __INDEX_GET(type)                                       \
    int clib_bst_index_##type##_get(                            \
            CLIB_BST *b, type *res, void *index) {              \
        type *temp = NULL;                                      \
        int ret = clib_bst_index_get_free(b, (void **)&temp, index);\
        if (temp) {*res = *temp;}                               \
        return ret;                                             \
    }

#define __INDEX_POINTER(type)                                   \
    type *clib_bst_simple_##type##_pointerize(type data) {      \
        type *ret = malloc(sizeof(data));                       \
        if (ret) {*ret = data;}                                 \
        return ret;                                             \
    }

#define __DECLARE(type)                                         \
    __INDEX_CMP(type)                                           \
    __INDEX_GET(type)                                           \
    __INDEX_POINTER(type)

__DECLARE(int)
