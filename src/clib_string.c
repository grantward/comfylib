//Provides common string manipulation functionality

#include <string.h>

#include "clib_string.h"


//If necessary, grows string storage to add requested length
static void _growable_resize(CLIB_STRING_GROWABLE *s, size_t add) {
    if (s->length + add >= s->capacity) {
        //Resize necessary
        size_t new_cap = s->capacity * 2;
        void *temp = realloc(s->string, new_cap);
        if (!temp)
            return;
        s->capacity = new_cap;
        s->string = temp;
    }
}

//Appends str to growable string
static void _growable_add(CLIB_STRING_GROWABLE *s, char *str) {
    strcat(s->string, str);
    s->length = strlen(s->string);
}

CLIB_STRING_GROWABLE *clib_string_growable_create(void) {
    struct clib_string_growable *ret = calloc(1, sizeof(*ret));
    if (!ret)
        return ret;
    ret->capacity = CLIB_STRING_DEFAULT_CAPACITY;
    ret->string = calloc(ret->capacity, sizeof(*(ret->string)));
    if (!ret->string) {
        free(ret);
        return NULL;
    }
    return ret;
}

char *clib_string_growable_extract(CLIB_STRING_GROWABLE *s) {
    if (!s)
        return NULL;
    char *ret = s->string;
    free(s);
    return ret;
}

void clib_string_growable_destroy(CLIB_STRING_GROWABLE *s) {
    if (!s)
        return;
    free(s->string);
    free(s);
}

int clib_string_growable_append(CLIB_STRING_GROWABLE *s, char *str) {
    if (!s || !str)
        return CLIB_ERR_NULL;
    _growable_resize(s, strlen(str));
    _growable_add(s, str);

    return 0;
}
