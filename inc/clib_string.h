#ifndef CLIB_STRING_H
#define CLIB_STRING_H

#include "clib.h"

#define CLIB_STRING_DEFAULT_CAPACITY        256

typedef struct clib_string_growable {
    char *string;

    size_t length;
    size_t capacity;
} CLIB_STRING_GROWABLE;

CLIB_STRING_GROWABLE *clib_string_growable_create(void);

char *clib_string_growable_extract(CLIB_STRING_GROWABLE *s);

void clib_string_growable_destroy(CLIB_STRING_GROWABLE *s);

int clib_string_growable_append(CLIB_STRING_GROWABLE *s, char *str);


#endif
