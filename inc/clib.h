#ifndef CLIB_H
#define CLIB_H

#include <stdlib.h>


#define CLIB_ERR_NULL           -2
#define CLIB_ERR_MEM            -3
#define CLIB_ERR_INDEX          -4

#define CLIB_SUCCESS            0
#define CLIB_RES_FOUND          CLIB_SUCCESS
#define CLIB_RES_NOT_FOUND      2

//CLIB LIST

typedef struct clib_list_node {
    struct clib_list_node *prev;
    struct clib_list_node *next;

    void *data;
} CLIB_LIST_NODE;

typedef struct clib_list {
    struct clib_list_node *first;
    struct clib_list_node *last;

    ssize_t len;

    //Passed on create node to copy/malloc new data
    void *(*copy_data)(void *);
    //Passed to node on cleanup to call on contained data
    void (*destroy_data)(void *);
} CLIB_LIST;

//Allocate and return generic list node
CLIB_LIST_NODE *clib_list_node_create(void *data, void *(*copy_data)(void *));
//Allocate and return generic list of generic nodes
CLIB_LIST *clib_list_create(
        void *(*copy_data)(void *),
        void (*destroy_data)(void *));

void clib_list_node_destroy(CLIB_LIST_NODE *n, void (*destroy_data)(void *));
void clib_list_destroy(CLIB_LIST *l);

//Inserts a new node containing data at index
//Index can be negative
int clib_list_insert(CLIB_LIST *l, ssize_t index, void *data);

//Inserts new node at end of list
int clib_list_append(CLIB_LIST *l, void *data);
//Inserts new node at beginning of list
int clib_list_appendleft(CLIB_LIST *l, void *data);

//Remove node at index and set res to data
int clib_list_pop_index(CLIB_LIST *l, void **res, ssize_t index);
int clib_list_pop(CLIB_LIST *l, void **res);
int clib_list_popleft(CLIB_LIST *l, void **res);

//Remove node at index
int clib_list_node_delete(CLIB_LIST *l, ssize_t index);

ssize_t clib_list_len(CLIB_LIST *l);

//Stores the data at index in res
//Returns a code indicating error or success
//Index can be negative in which case it goes end-beginning
//For example index -1 is the last item in the list
int clib_list_index(CLIB_LIST *l, void **res, ssize_t index);
//Return the node at given index (used by above)
CLIB_LIST_NODE *clib_list_get_node(CLIB_LIST *l, ssize_t index);

//Call func on each item in list
void clib_list_iterate(CLIB_LIST *l, void (*func)(void *));

//Build and return a new list based on results of func on each node
CLIB_LIST *clib_list_map(
        CLIB_LIST *l,
        void *(*func)(const void *),
        void *(*copy_data)(void *),
        void (*destroy_data)(void *));

//Build and return a JSON string compiled from the result of func
//on each node
//Func must return a malloc'd string of valid JSON
//Func can return NULL or blank string to indicate no value
char *clib_list_to_json(CLIB_LIST *l, char *(*func)(void *));

//Take in a sum function and a location to store result
//Run sum func against each node and store the result
void *clib_list_sum(CLIB_LIST *l, void *(*sum_func)(void *, void *),
        void *res);

//Take in a sum function and division function
//Return a result representing mean value in list
void *clib_list_mean(
        CLIB_LIST *l,
        void *(*sum_func)(void *,void *),
        void *(*div_func)(void *,ssize_t),
        void *res);

//CLIB GRAPH

typedef struct clib_graph {
    struct clib_list *nodes;
    
} CLIB_GRAPH;

CLIB_GRAPH *clib_graph_create(void);

//Additional modules
#include "clib_bst.h"
#include "clib_dynamic.h"
#include "clib_list_algo.h"
#include "clib_string.h"
#include "clib_test.h"
//Additional list types
#include "clib_list_shallow.h"
//Specific typed lists
#include "clib_list_string.h"
//Generated code definitions
#include "clib_list_conv_func.h"

#endif
