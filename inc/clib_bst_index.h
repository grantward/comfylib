#ifndef CLIB_BST_INDEX_H
#define CLIB_BST_INDEX_H

//Container for indexed data in a BST
typedef struct clib_bst_index_data {
    void *index;
    void *data;
} CLIB_BST_INDEX_DATA;

//Constant variant of above for transfer
struct clib_bst_index_data_c {
    const void *index;
    const void *data;
    size_t index_size;
    size_t data_size;
};

//Create a simple index data BST
#define clib_bst_index_create(cmp_index)                            \
    clib_bst_create(                                                \
            (void *)clib_bst_index_copy,                            \
            (void *)clib_bst_index_destroy,                         \
            (void *)cmp_index)

struct clib_bst_index_data *clib_bst_index_copy(
        const struct clib_bst_index_data_c *data);

void clib_bst_index_destroy(CLIB_BST_INDEX_DATA *data);

//Build a container around indexed data and add to BST
int clib_bst_index_add(CLIB_BST *b,
        size_t index_size, const void *index,
        size_t data_size, const void *data);

//See above but assumes allocated index/data and frees after adding
int clib_bst_index_add_free(CLIB_BST *b,
        size_t index_size, void *index,
        size_t data_size, void *data);

int clib_bst_index_get(CLIB_BST *b, void **res, void *index);
int clib_bst_index_get_free(CLIB_BST *b, void **res, void *index);


#endif
