#ifndef CLIB_LIST_ALGO_H
#define CLIB_LIST_ALGO_H

//Selection algorithm (quickselect)
//To understand cmp function, see strcmp. Same format.
//k being the kth-element from least
//ie. k=0 indicates the lowest (minimum) value
int clib_list_quickselect(
        CLIB_LIST *l, void **res, int (*cmp)(void *, void *), size_t k);

//Return the minimum value in list
int clib_list_min(CLIB_LIST *l, void **res, int (*cmp)(void *, void *));
//See above
int clib_list_max(CLIB_LIST *l, void **res, int (*cmp)(void *, void *));

//Return the median of list
//If list length is even, the mean of medianhigh/medianlow is returned
//Res will normally be malloc'd
int clib_list_median(
        CLIB_LIST *l, void **res,
        int (*cmp)(void *,void *),
        void *(*add)(void *,void *),
        void *(*div)(void *,ssize_t));

//Return the low median (no mean taken) of list
int clib_list_medianlow(
	    CLIB_LIST *l, void **res, int (*cmp)(void *,void *));
//See above
int clib_list_medianhigh(
	    CLIB_LIST *l, void **res, int (*cmp)(void *,void *));




#endif
