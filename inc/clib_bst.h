#ifndef CLIB_BST_H
#define CLIB_BST_H

#include <stdlib.h>

//Array of NULL-terminated functions
typedef void **CLIB_FUNC_LIST;

typedef struct clib_bst_node {
    struct clib_bst_node *parent;
    struct clib_bst_node *left;
    struct clib_bst_node *right;

    void *data;
} CLIB_BST_NODE;

typedef struct clib_bst {
    struct clib_bst_node *root;

    //Function to copy (ie. allocate) data to new node
    void *(*copy_data)(const void *);
    //Function to cleanup data
    void (*destroy_data)(void *);
    //Function to compare data
    //See strcmp to understand format
    int (*cmp_data)(const void *,const void *);
} CLIB_BST;

CLIB_BST_NODE *clib_bst_node_create(
        const void *data, void *(*copy_data)(const void *));

CLIB_BST *clib_bst_create(
        void *(*copy_data)(const void *),
        void (*destroy_data)(void *),
        int (*cmp_data)(const void *,const void *));

void clib_bst_node_destroy(CLIB_BST_NODE *n, void (*destroy_data)(void *));

void clib_bst_destroy(CLIB_BST *b);

//Creates a new node then
//finds location of new node in tree and adds it
int clib_bst_add_node(CLIB_BST *b, const void *data);

//Find and return data if in tree
int clib_bst_get(CLIB_BST *b, void **res, const void *data);

//Find and return node corresponding to data
CLIB_BST_NODE *clib_bst_get_node(CLIB_BST *b, const void *data);

//Return the would-be parent node of data not currently in tree
CLIB_BST_NODE *clib_bst_get_parent(CLIB_BST *b, const void *data);

//Include sub modules
#include "clib_bst_simple.h"

#endif
