#ifndef CLIB_SIMPLE_H
#define CLIB_SIMPLE_H

#include <stdint.h>
#include <string.h>

#include "clib.h"

//Values for conversion to string
#define CLIB_SIMPLE_STRING_LEN              64
#define CLIB_SIMPLE_int_str                 "%d"
#define CLIB_SIMPLE_long_str                "%ld"
#define CLIB_SIMPLE_size_t_str              "%zu"
#define CLIB_SIMPLE_ssize_t_str             "%zi"
#define CLIB_SIMPLE_float_str               "%f"
#define CLIB_SIMPLE_double_str              "%lf"
#define CLIB_SIMPLE_uint64_t_str            "%lu"
#define CLIB_SIMPLE_uint32_t_str            "%u"
#define CLIB_SIMPLE_uint16_t_str            CLIB_SIMPLE_uint32_t_str
#define CLIB_SIMPLE_uint8_t_str             CLIB_SIMPLE_uint32_t_str
#define CLIB_SIMPLE_int64_t_str             CLIB_SIMPLE_long_str
#define CLIB_SIMPLE_int32_t_str             CLIB_SIMPLE_int_str
#define CLIB_SIMPLE_int16_t_str             CLIB_SIMPLE_int_str
#define CLIB_SIMPLE_int8_t_str              CLIB_SIMPLE_int_str


//Simple string size for compatibility with numeric types
#define clib_simple_string_size(str) (strlen(str)+1)

#define __TYPE_SIZE(type)                   \
    size_t clib_simple_##type##_size(type data);

#define __TYPE_COPY(type)                   \
    void *clib_simple_##type##_copy(type *data);

#define __TYPE_STR(type)                    \
    char *clib_simple_##type##_str(type *data);

#define __DECLARE(type)                     \
    __TYPE_SIZE(type)                       \
    __TYPE_COPY(type)                       \
    __TYPE_STR(type)

__DECLARE(int)
__DECLARE(long)
__DECLARE(float)
__DECLARE(double)

__DECLARE(uint8_t)
__DECLARE(uint16_t)
__DECLARE(uint32_t)
__DECLARE(uint64_t)
__DECLARE(int8_t)
__DECLARE(int16_t)
__DECLARE(int32_t)
__DECLARE(int64_t)

#undef __TYPE_SIZE
#undef __TYPE_COPY
#undef __TYPE_STR
#undef __DECLARE

#endif
