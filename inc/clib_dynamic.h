#ifndef CLIB_DYNAMIC_H
#define CLIB_DYNAMIC_H

#include <stdint.h>

#include "clib.h"

#define CLIB_DYNAMIC_ERR                0xFFFF
#define CLIB_DYNAMIC_INT                0x1001
#define CLIB_DYNAMIC_UINT               0x1002
#define CLIB_DYNAMIC_FLOAT              0x2001
#define CLIB_DYNAMIC_STR                0x3001

typedef struct clib_dynamic_t {
    uint32_t code;
    union {
        void *data;
        int64_t data_err;
        int64_t data_int;
        uint64_t data_uint;
        double data_float;
        char *data_str;
    };
} CLIB_DYNAMIC_T;

void clib_dynamic_destroy(CLIB_DYNAMIC_T *d);

CLIB_DYNAMIC_T *clib_dynamic_str_create(char *str);

#define __err_type      int64_t
#define __int_type      int64_t
#define __uint_type     uint64_t
#define __float_type    double

#define __CREATE_TYPE(type)     \
    CLIB_DYNAMIC_T *clib_dynamic_##type##_create(__##type##_type data);

#define __REGISTER_TYPE(type)   \
    __CREATE_TYPE(type)

__REGISTER_TYPE(err)
__REGISTER_TYPE(int)
__REGISTER_TYPE(uint)
__REGISTER_TYPE(float)

#undef __err_type
#undef __int_type
#undef __uint_type
#undef __float_type
#undef __CREATE_TYPE
#undef __REGISTER_TYPE


#endif
