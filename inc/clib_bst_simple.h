#ifndef CLIB_BST_SIMPLE_H
#define CLIB_BST_SIMPLE_H

#include <string.h>

#include "clib_bst.h"
#include "clib_bst_index.h"

//Duplicates a string for compatibility with existing
//simple data type architecture
#define clib_bst_simple_string_pointerize(str) strdup(str)

//Create and return a BST mapping simple index_type to data_type
#define clib_bst_index_simple_create(index_type)                \
    clib_bst_index_create(clib_bst_index_##index_type##_cmp)

//Strcmp of string index
int clib_bst_index_string_cmp(
        const CLIB_BST_INDEX_DATA *container_a,
        const CLIB_BST_INDEX_DATA *container_b);

//Retrieval of string data from indexed BST
int clib_bst_index_string_get(CLIB_BST *b, char **res, void *index);

//Add simple data to BST
#define clib_bst_index_simple_add(bst, index_type, index, data_type, data)\
    clib_bst_index_add_free(                                    \
            bst,                                                \
            clib_simple_##index_type##_size(index),         \
            clib_bst_simple_##index_type##_pointerize(index),   \
            clib_simple_##data_type##_size(data),           \
            clib_bst_simple_##data_type##_pointerize(data))

//Retrieve data of specified data,index types from BST
#define clib_bst_index_simple_get(                              \
        bst, data_type, data_container, index_type, index)      \
    clib_bst_index_##data_type##_get(                           \
            bst,                                                \
            &data_container,                                    \
            clib_bst_simple_##index_type##_pointerize(index))

#define __INDEX_CMP(type)                                       \
    int clib_bst_index_##type##_cmp(                            \
            const CLIB_BST_INDEX_DATA *container_a,             \
            const CLIB_BST_INDEX_DATA *container_b);

#define __INDEX_GET(type)                                       \
    int clib_bst_index_##type##_get(                            \
            CLIB_BST *b, type *res, void *index);

#define __INDEX_POINTER(type)                                   \
    type *clib_bst_simple_##type##_pointerize(type data);       

#define __DECLARE(type)                                         \
    __INDEX_CMP(type)                                           \
    __INDEX_GET(type)                                           \
    __INDEX_POINTER(type)

__DECLARE(int)

#undef __INDEX_CMP
#undef __INDEX_GET
#undef __INDEX_POINTER
#undef __DECLARE

#endif
