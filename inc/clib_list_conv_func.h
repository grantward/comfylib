#ifndef CLIB_LIST_CONV_FUNC_H
#define CLIB_LIST_CONV_FUNC_H

#include <stdint.h>

#include "clib.h"
#include "clib_simple.h"


#define _CLIB__TYPE_INSERT(type)    \
    int clib_list_##type##_insert(CLIB_LIST *l, ssize_t index, type data);

#define _CLIB__TYPE_APPEND(type)    \
    int clib_list_##type##_append(CLIB_LIST *l, type data);

#define _CLIB__TYPE_APPENDLEFT(type)\
    int clib_list_##type##_appendleft(CLIB_LIST *l, type data);

#define _CLIB__TYPE_POP_INDEX(type) \
    int clib_list_##type##_pop_index(CLIB_LIST *l, type *res, ssize_t index);
#define _CLIB__TYPE_POP(type)       \
    int clib_list_##type##_pop(CLIB_LIST *l, type *res);
#define _CLIB__TYPE_POPLEFT(type)   \
    int clib_list_##type##_popleft(CLIB_LIST *l, type *res);

#define _CLIB__TYPE_CREATE(type)    \
    CLIB_LIST *clib_list_##type##_create(void);

#define _CLIB__TYPE_INDEX(type)     \
    int clib_list_##type##_index(CLIB_LIST *l, type *res, ssize_t index);

#define _CLIB__TYPE_MAP(type)       \
    CLIB_LIST *clib_list_##type##_map(CLIB_LIST *l, type (*func)(void *));

#define _CLIB__TYPE_TO_JSON(type)   \
    char *clib_list_##type##_to_json(CLIB_LIST *l);

#define _CLIB__TYPE_QUICKSELECT(type)\
    int clib_list_##type##_quickselect(CLIB_LIST *l, type *res, size_t k);\
    int clib_list_##type##_min(CLIB_LIST *l, type *res);\
    int clib_list_##type##_max(CLIB_LIST *l, type *res);

#define _CLIB__TYPE_MEDIAN(type)    \
    int clib_list_##type##_median(CLIB_LIST *l, type *res);

#define _CLIB__TYPE_SUM(type)       \
    type clib_list_##type##_sum(CLIB_LIST *l);

#define _CLIB__TYPE_MEAN(type)      \
    type clib_list_##type##_mean(CLIB_LIST *l);

#define _CLIB__TYPE_DECLARE(type)   \
    _CLIB__TYPE_INSERT(type)        \
    _CLIB__TYPE_APPEND(type)        \
    _CLIB__TYPE_APPENDLEFT(type)    \
    _CLIB__TYPE_POP_INDEX(type)     \
    _CLIB__TYPE_POP(type)           \
    _CLIB__TYPE_POPLEFT(type)       \
    _CLIB__TYPE_CREATE(type)        \
    _CLIB__TYPE_INDEX(type)         \
    _CLIB__TYPE_MAP(type)           \
    _CLIB__TYPE_TO_JSON(type)       \
    _CLIB__TYPE_QUICKSELECT(type)   \
    _CLIB__TYPE_MEDIAN(type)        \
    _CLIB__TYPE_SUM(type)           \
    _CLIB__TYPE_MEAN(type)

//Types
_CLIB__TYPE_DECLARE(int)
_CLIB__TYPE_DECLARE(long)
_CLIB__TYPE_DECLARE(float)
_CLIB__TYPE_DECLARE(double)

_CLIB__TYPE_DECLARE(uint8_t)
_CLIB__TYPE_DECLARE(uint16_t)
_CLIB__TYPE_DECLARE(uint32_t)
_CLIB__TYPE_DECLARE(uint64_t)
_CLIB__TYPE_DECLARE(int8_t)
_CLIB__TYPE_DECLARE(int16_t)
_CLIB__TYPE_DECLARE(int32_t)
_CLIB__TYPE_DECLARE(int64_t)


#undef _CLIB__TYPE_INSERT
#undef _CLIB__TYPE_APPEND
#undef _CLIB__TYPE_APPENDLEFT
#undef _CLIB__TYPE_POP_INDEX
#undef _CLIB__TYPE_POP
#undef _CLIB__TYPE_POPLEFT
#undef _CLIB__TYPE_CREATE
#undef _CLIB__TYPE_INDEX
#undef _CLIB__TYPE_MAP
#undef _CLIB__TYPE_TO_JSON
#undef _CLIB__TYPE_SUM
#undef _CLIB__TYPE_QUICKSELECT
#undef _CLIB__TYPE_MEDIAN
#undef _CLIB__TYPE_MEAN
#undef _CLIB__TYPE_DECLARE

#endif
