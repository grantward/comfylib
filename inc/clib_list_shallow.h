#ifndef CLIB_LIST_SHALLOW_H
#define CLIB_LIST_SHALLOW_H

#include "clib.h"


//Provides ALL the elements, not just those that returned true
struct clib_list_shallow_true_false {
    CLIB_LIST *true;
    CLIB_LIST *false;
};

//Provides list based on cmp func
struct clib_list_shallow_cmp {
    CLIB_LIST *lesser;
    CLIB_LIST *equal;
    CLIB_LIST *greater;
};

//Returns a shallow copy (same data) list from input list
CLIB_LIST *clib_list_shallow_copy(CLIB_LIST *l);

//Returns a shallow copy of all elements from list for which
//func returns true
CLIB_LIST *clib_list_shallow_copy_func(CLIB_LIST *l, int (*func)(void *));

//See above but include a constant argument to func
CLIB_LIST *clib_list_shallow_copy_func_1(
        CLIB_LIST *l,
        int (*func)(void *, void *),
        void *arg_1);

//See above but returning both the true and false lists
struct clib_list_shallow_true_false clib_list_shallow_copy_func_1_both(
        CLIB_LIST *l,
        int (*func)(void *, void *),
        void *arg_1);

//See above but binning lists into lesser, equal, greater
struct clib_list_shallow_cmp clib_list_shallow_copy_func_1_cmp(
        CLIB_LIST *l,
        int (*cmp)(void *, void *),
        void *arg_1);


#endif
