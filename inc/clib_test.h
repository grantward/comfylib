#ifndef CLIB_TEST_H
#define CLIB_TEST_H

#include "clib.h"

struct clib_test {
    //0 - Fail
    //1 - Pass
    int success;
    char *test_name;
};

CLIB_LIST *clib_test_create(void);

//Success if expr_1 == expr_2
int clib_test_equal(CLIB_LIST *l, char *test_name,
        int expr_1, int expr_2);

void clib_test_print_results(CLIB_LIST *l);


#endif
