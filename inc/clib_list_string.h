#ifndef CLIB_LIST_STRING_H
#define CLIB_LIST_STRING_H

#include "clib.h"

CLIB_LIST *clib_list_string_create(void);

//Print each string in list
void clib_list_string_print(CLIB_LIST *l);


#endif
