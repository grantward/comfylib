CFLAGS := -Wall -Wextra
MKDIR := mkdir -p

INC_DIR := inc
OUT_DIR := build bin bin/test build/test
BIN_SRC := $(wildcard *.c)
TEST_BIN_SRC := $(wildcard test/*.c)
LIB_SRC := $(wildcard src/*.c)

INC := $(addprefix -I,$(INC_DIR))
LIB_OBJ := $(patsubst src/%.c,build/%.o,$(LIB_SRC))
BIN_OBJ := $(patsubst %.c,build/%.o,$(BIN_SRC))
BIN := $(patsubst %.c,bin/%,$(BIN_SRC) $(TEST_BIN_SRC))
ARTIFACTS := gmon.out

.PHONY: all profile debug print-% clean

all: $(BIN)

profile: CFLAGS += -pg -g -no-pie
profile: all

debug: CFLAGS += -g
debug: all

print-%:
	@echo $* = $($*)

clean:
	$(RM) -r bin
	$(RM) -r build
	$(RM) $(ARTIFACTS)


$(LIB_OBJ): | $(OUT_DIR)
$(BIN): | $(OUT_DIR)

$(OUT_DIR):
	$(MKDIR) $(OUT_DIR)

build/%.o: src/%.c
	$(CC) $(CFLAGS) $(INC) -c -o $@ $^

bin/%: $(LIB_OBJ)
	$(CC) $(CFLAGS) $(INC) -c -o build/$(@F).o $(@F).c
	$(CC) $(CFLAGS) $(INC) -o $@ build/$(@F).o $^

bin/test/%: $(LIB_OBJ)
	$(CC) $(CFLAGS) $(INC) -c -o build/test/$(@F).o test/$(@F).c
	$(CC) $(CFLAGS) $(INC) -o $@ build/test/$(@F).o $^
