//Test functionality of comfylib BST structure

#include <stdio.h>
#include <string.h>

#include "clib.h"

int main(void) {
    CLIB_LIST *tests = clib_test_create();
    //Create a BST mapping ints to ints
    //ie. {5: 10}
    CLIB_BST *b = clib_bst_index_simple_create(int);
    int res = clib_bst_index_simple_add(b, int, 5, int, 10);
    clib_test_equal(tests, "Test add indexed data to BST", res, 0);


    int container = 0;
    res = clib_bst_index_simple_get(b, int, container, int, 5);
    clib_test_equal(tests, "Test get indexed data succeed", res, 0);
    clib_test_equal(tests, "Test get indexed data correct", container, 10);

    CLIB_BST *str = clib_bst_index_simple_create(string);
    clib_bst_index_simple_add(str, string, "color", string, "red");
    clib_bst_index_simple_add(str, string, "size", string, "large");

    char *str_container;
    clib_bst_index_simple_get(str, string, str_container, string, "color");
    clib_test_equal(tests, "Test get string indexed string correct",
            strcmp(str_container, "red"), 0);


    clib_test_print_results(tests);

    clib_bst_destroy(b);
    clib_bst_destroy(str);
    clib_list_destroy(tests);
    return 0;
}
