//Evaluate functionality of clib

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "clib.h"

int main(void) {
    CLIB_LIST *tests = clib_test_create();
    CLIB_LIST *l = clib_list_long_create();

    clib_test_equal(tests, "Initial length", 0, clib_list_len(l));

    for (long i=0; i<1000; i++) {
        clib_list_long_append(l, i);
    }

    puts("==== TEST 1000 integer list JSON ====");
    char *str = clib_list_long_to_json(l);
    puts(str);
    free(str);

    CLIB_LIST *l_copy = clib_list_shallow_copy(l);

    puts("==== TEST 15 float list JSON ====");
    CLIB_LIST *float_list = clib_list_double_create();
    for (double i=0.5; i<15; i += 1) {
        clib_list_double_append(float_list, i);
    }
    str = clib_list_double_to_json(float_list);
    puts(str);
    free(str);

    clib_test_equal(
            tests, "1000 longs length test", 1000, clib_list_len(l));
    clib_test_equal(
            tests,
            "1000 longs copy length test",
            1000,
            clib_list_len(l_copy));
    clib_list_destroy(l_copy);
    clib_test_equal(
            tests,
            "15 floats length test",
            15,
            clib_list_len(float_list));

    long temp;
    clib_list_long_index(l, &temp, 0);
    clib_test_equal(tests, NULL, 0, temp);

    clib_list_long_index(l, &temp, 2);
    clib_test_equal(tests, NULL, 2, temp);

    clib_list_long_max(l, &temp);
    clib_test_equal(tests, "Max value test", 999, temp);

    clib_list_long_min(l, &temp);
    clib_test_equal(tests, "Min value test", 0, temp);
    
    clib_test_print_results(tests);
    clib_list_destroy(tests);
    clib_list_destroy(l);
    clib_list_destroy(float_list);
    return 0;
}
