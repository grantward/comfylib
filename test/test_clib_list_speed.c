//Evaluates the speed to build a 10000 int list and find min and max

#include <stdio.h>

#include "clib.h"


int main(void) {
    //CLIB_LIST *ints = clib_list_int_create();
    //for (int i=0; i<10000; i++) {
    //    clib_list_int_append(ints, i);
    //}
    //int temp;
    //clib_list_int_min(ints, &temp);
    //printf("Min: %d\n", temp);
    //clib_list_int_max(ints, &temp);
    //printf("Max: %d\n", temp);
    //clib_list_int_median(ints, &temp);
    //printf("Median: %d\n", temp);
    //clib_list_destroy(ints);

    CLIB_LIST *floats = clib_list_double_create();
    for (double i=0.5;i<10000000;i+=1) {
        clib_list_double_append(floats, i);
    }
    double temp;

    clib_list_double_min(floats, &temp);
    printf("Min: %lf\n", temp);
    clib_list_double_max(floats, &temp);
    printf("Max: %lf\n", temp);
    //clib_list_double_median(floats, &temp);
    //printf("Median: %lf\n", temp);

    clib_list_destroy(floats);
    return 0;
}
